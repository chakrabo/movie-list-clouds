import { initializeApp } from 'https://www.gstatic.com/firebasejs/9.6.3/firebase-app.js';
import { getAnalytics } from "https://www.gstatic.com/firebasejs/9.6.3/firebase-analytics.js";
import { getAuth, GoogleAuthProvider, signInWithPopup } from "https://www.gstatic.com/firebasejs/9.6.3/firebase-auth.js";
import { getDatabase } from "https://www.gstatic.com/firebasejs/9.6.3/firebase-database.js";
import { getFirestore } from "https://www.gstatic.com/firebasejs/9.6.3/firebase-firestore.js";

// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
    apiKey: "AIzaSyBdZof5WfvXIT7XukAwXZo6z5R9MjucsFc",
    authDomain: "movielist-7c61f.firebaseapp.com",
    databaseURL: "https://movielist-7c61f-default-rtdb.firebaseio.com",
    projectId: "movielist-7c61f",
    storageBucket: "movielist-7c61f.appspot.com",
    messagingSenderId: "41418251168",
    appId: "1:41418251168:web:8ee3ea14be3c1618da62c5",
    measurementId: "G-6NLLHN94JH"
  };


// Initialize Firebase
const app = initializeApp(firebaseConfig);
const analytics = getAnalytics(app);

const provider = new GoogleAuthProvider();
provider.setCustomParameters({ prompt: "select_account" });

 const auth = getAuth();
 const signInWithGoogle = () => signInWithPopup(auth, provider);
 const db = getDatabase(app);
 const dbFirestore = getFirestore(app);




window.onload = () =>  {
    checkLogin();
    const login = document.querySelector('#login');

    login.onclick = () => GoogleLogin();
}


function checkLogin(){
    auth.onAuthStateChanged((user) => {
        if(user){
            alert("Redirecting to dashboard...");
            window.location.href = "./frontpage.html";
        }
    })

}

function GoogleLogin(){
	
    signInWithGoogle()
    .then((result) => {
        const credential = GoogleAuthProvider.credentialFromResult(result);
        const token = credential.accessToken;
        const user = result.user;
        console.log(user);
        window.location.href = "./frontpage.html";
    }).catch((error) => { 
        const errorCode = error.code;
        const errorMessage = error.message;
        const email = error.email;
        const credential = GoogleAuthProvider.credentialFromError(error);
		console.log(error);
  });
}

