
import {ref, set, get, child} from "https://www.gstatic.com/firebasejs/9.6.3/firebase-database.js";
import {doc, updateDoc, deleteField, getDoc, setDoc} from "https://www.gstatic.com/firebasejs/9.6.3/firebase-firestore.js";
import { initializeApp } from 'https://www.gstatic.com/firebasejs/9.6.3/firebase-app.js';
import { getAnalytics } from "https://www.gstatic.com/firebasejs/9.6.3/firebase-analytics.js";
import { getAuth, GoogleAuthProvider, signInWithPopup } from "https://www.gstatic.com/firebasejs/9.6.3/firebase-auth.js";
import { getDatabase } from "https://www.gstatic.com/firebasejs/9.6.3/firebase-database.js";
import { getFirestore } from "https://www.gstatic.com/firebasejs/9.6.3/firebase-firestore.js";

// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
    apiKey: "AIzaSyBdZof5WfvXIT7XukAwXZo6z5R9MjucsFc",
    authDomain: "movielist-7c61f.firebaseapp.com",
    databaseURL: "https://movielist-7c61f-default-rtdb.firebaseio.com",
    projectId: "movielist-7c61f",
    storageBucket: "movielist-7c61f.appspot.com",
    messagingSenderId: "41418251168",
    appId: "1:41418251168:web:8ee3ea14be3c1618da62c5",
    measurementId: "G-6NLLHN94JH"
  };

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const analytics = getAnalytics(app);

const provider = new GoogleAuthProvider();
provider.setCustomParameters({ prompt: "select_account" });

 const auth = getAuth();
 const signInWithGoogle = () => signInWithPopup(auth, provider);
 const db = getDatabase(app);
 const dbFirestore = getFirestore(app);
window.onload = () =>  {
    checkLogin();
}

var flagLogout = false;
function checkLogin(){
    auth.onAuthStateChanged((user) => {
        if(user){

            document.querySelector('#username').innerHTML = user.displayName;

            document.querySelector("#logout").onclick = () => {
                flagLogout = true;
                auth.signOut().then(function() {
                    flagLogout = true;
                    console.log("Signed out");
                } , function(error) {
                    console.error("Sign out Error", error);
                })
            }
			var refnow = window.location.href;
			if(refnow.includes("frontpage")){
				setPage(user.uid);
			}
            else {
				setWishlist(user.uid);
			}
        }
        else{
            if(!flagLogout)
                alert("Please log in first!");
            window.location.href = "index.html";
        }
		console.log(window.location.href);
    })

}

async function addToWishList(uid, data){
    // movie's fields
    var mid = data.id; 
    var title = data.title;
    var year = data.year;
    var genre = data.genre;

    var node = document.querySelector("#msg_displayed");
    node.innerHTML = "Great choice! Adding to your list...";
    var msg = document.querySelector("#msg_load");
    msg.setAttribute("style","visibility:visible;");

    set(ref(db, uid + "/" + mid), {
        "genre": genre,
        "id": mid,
        "title": title,
        "year": year,
        "inWishList": true
    })
    
    var userDocRef = doc(dbFirestore, "wishlist", uid);
    var userDocSnap = await getDoc(userDocRef);
    if(userDocSnap.exists()){
        updateDoc(userDocRef, {
            [mid]: {"genre": genre, "id": mid, "title": title, "year": year} 
        })
    }
    else{
        setDoc(doc(dbFirestore, "wishlist", uid), {
            [mid]: {"genre": genre, "id": mid, "title": title, "year": year} 
        })
    }

    msg.setAttribute("style","visibility:hidden;");
    var rowId = document.querySelector('#button_table_' + mid).parentNode.parentNode.rowIndex;
    document.querySelector("#table_movies").deleteRow(rowId);
}

async function removeFromWishList(uid, data){

    var mid = data.id; 
    var title = data.title;
    var year = data.year;
    var genre = data.genre;

    var node = document.querySelector("#msg_displayed");
    node.innerHTML = "Okay! Removing from your list...";
    var msg = document.querySelector("#msg");
    msg.setAttribute("style","visibility:visible;");

    set(ref(db, uid + "/" + mid), {
        "genre": genre,
        "id": mid,
        "title": title,
        "year": year,
        "inWishList": false
    })
    
    const userMoviesRef = doc(dbFirestore, "wishlist", uid);
    await updateDoc(userMoviesRef, {
        [mid]: deleteField()
    });

    msg.setAttribute("style","visibility:hidden;");
    var rowId = document.querySelector('#button_table_' + mid).parentNode.parentNode.rowIndex;
    document.querySelector("#table_movies").deleteRow(rowId);

    const docRef = doc(dbFirestore, "wishlist", uid);
    const docSnap = await getDoc(docRef);
    if((Object.keys(docSnap.data()).length) == 0){
        document.querySelector("#container").setAttribute("style", "visibility: hidden;");
        emptyWishList();
        document.querySelector("#msg").setAttribute("style", "visibility: visible;");
    }
}

function setPage(uid_user){

    var rtdata = ref(db);
    get(child(rtdata, ""+uid_user)).then((snapshot) => {
        if(snapshot.exists()){
            // User already logged in
            var table = document.querySelector('#table_movies');
            var container = document.querySelector('#container');
            var data = snapshot.val(); 
            for(let i in data){
                // check if the movie is already in wishlist (in case, don't display it)
                if(!data[i].inWishList){
                    var row = table.insertRow(-1);
                    row.insertCell(0).innerHTML = data[i].title;
                    row.insertCell(1).innerHTML = data[i].year;
                    row.insertCell(2).innerHTML = data[i].genre;
                    // add the button for wishlist
                    var currId = "button_table_" + i;
                    row.insertCell(3).innerHTML = '<button id=' + currId + ' class="heart"><img src="img/rating.png" height="20px"></button>';
                    document.querySelector('#' + currId).onclick = () => addToWishList(uid_user, data[i]);
                } 
            }
            document.querySelector("#msg_load").setAttribute("style", "visibility: hidden");
            container.setAttribute("style", "visibility: visible;");
        }
        else {
            // First ever login for this user
            addNewUser(uid_user);
          }
        }).catch((error) => {
          console.error(error);
        });
                
} 

async function setWishlist(uid_user){

    const docRef = doc(dbFirestore, "wishlist", uid_user);
    const docSnap = await getDoc(docRef);
    var table = document.querySelector('#table_movies');
    var container = document.querySelector('#container');
    var flag;

    if(docSnap.exists()){
        flag = false;
        var cnt = 0;
        for(let mid in docSnap.data()){
            flag = true;
            cnt = cnt + 1;
            var movie = docSnap.data()[mid];
            var row = table.insertRow(-1);
            row.insertCell(0).innerHTML = movie.title;
            row.insertCell(1).innerHTML = movie.year;
            row.insertCell(2).innerHTML = movie.genre;
            var currId = "button_table_" + mid;
            row.insertCell(3).innerHTML = '<button id=' + currId + ' class="heart"><img src="img/remove-from-cart.png" style="height:20px;"></button>';
            document.querySelector('#' + currId).onclick = () => removeFromWishList(uid_user, docSnap.data()[mid]);
        }
        if(flag){
            document.querySelector("#msg").setAttribute("style", "visibility: hidden");
            container.setAttribute("style", "visibility: visible;");
        }
        else{
            emptyWishList();
        }
    }
    else{
        emptyWishList();
    }           
} 

function addNewUser(uid_user){
    var rtdata = ref(db);
    // get the movies from the movies list
    get(child(rtdata, "movies-list")).then((snapshot) => {
        if(snapshot.exists()){
            var data = snapshot.val(); 
            for(let i in data){
                var genre = data[i].genre;
                var id = data[i].id;
                var title = data[i].title;
                var year = data[i].year;
                // add record to the user's movie-list
                set(ref(db, uid_user + "/" + id), {
                    "genre": genre,
                    "id": id,
                    "title": title,
                    "year": year,
                    "inWishList": false
                })
                // then, display all the movies
                var table = document.querySelector('#table_movies');
                var container = document.querySelector('#container');
                var row = table.insertRow(-1);
                row.insertCell(0).innerHTML = title;
                row.insertCell(1).innerHTML = year;
                row.insertCell(2).innerHTML = genre;
                // add the button for wishlist
                var currId = "button_table_" + i;
                row.insertCell(3).innerHTML = '<button id=' + currId + ' class="heart"><img src="img/rating.png"></button>';
                document.querySelector('#' + currId).onclick = () => addToWishList(uid_user, data[i]);
            }
            document.querySelector("#msg_load").setAttribute("style", "visibility: hidden");
            container.setAttribute("style", "visibility: visible;");
        }
        else{
            console.log("Error: cannot get wishlist");
        }
    })
}

            
function searchTable() {
    
    var input = document.getElementById("searchBar");
    var filter = input.value.toUpperCase();
    var table = document.getElementById("table_movies");
    var tr = table.getElementsByTagName("tr");
    for (var i = 0; i < tr.length; i++) {
        var td = tr[i].getElementsByTagName("td")[0];
		var txtValue;
        if (td) {
            txtValue = td.textContent || td.innerText;
            if (txtValue.toUpperCase().indexOf(filter) > -1) {
                tr[i].style.display = "";
                continue;
            }
			else {
                tr[i].style.display = "none";
            }
        }  
        td = tr[i].getElementsByTagName("td")[1];
        if (td) {
            txtValue = td.textContent || td.innerText;
            if (txtValue.toUpperCase().indexOf(filter) > -1) {
                tr[i].style.display = "";
                continue;
            }
			else {
                tr[i].style.display = "none";
            }
        } 
        td = tr[i].getElementsByTagName("td")[2];
        if (td) {
            txtValue = td.textContent || td.innerText;
            if (txtValue.toUpperCase().indexOf(filter) > -1) {
                tr[i].style.display = "";
                continue;
            }
			else {
                tr[i].style.display = "none";
            }
        } 
    }
}
window.searchTable = searchTable;

function emptyWishList(){
    var node, nodeButton;
    node = document.querySelector("#msg_displayed");
    node.innerHTML = "Your list is empty!";
    nodeButton = document.createElement("button");
    nodeButton.setAttribute("id","btnEmptyWishList");
    nodeButton.setAttribute("onclick","location.href='frontpage.html';");
    nodeButton.innerHTML = "Movies list";
    document.querySelector("#msg").appendChild(nodeButton);
}

